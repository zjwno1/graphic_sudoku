﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace 图形数独
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private Label[,] array;
        private void Form1_Load(object sender, EventArgs e)
        {
            array = new Label[5, 5];
            array[0, 0] = label1; array[0, 1] = label2; array[0, 2] = label3; array[0, 3] = label4; array[0, 4] = label25;
            array[1, 0] = label5; array[1, 1] = label6; array[1, 2] = label7; array[1, 3] = label8; array[1, 4] = label26;
            array[2, 0] = label9; array[2, 1] = label10; array[2, 2] = label11; array[2, 3] = label12; array[2, 4] = label27;
            array[3, 0] = label13; array[3, 1] = label14; array[3, 2] = label15; array[3, 3] = label16; array[3, 4] = label28;
            array[4, 0] = label29; array[4, 1] = label30; array[4, 2] = label31; array[4, 3] = label32; array[4, 4] = label33;
            for (int i = 0; i < 5; i++)
            {
                for (int j = 0; j < 5; j++)
                {
                    array[i, j].Tag = i + "," + j;
                    array[i, j].Click += new System.EventHandler(this.Label1_Click);
                }
            }
            label18.Click += new System.EventHandler(this.Label2_Click);
            label19.Click += new System.EventHandler(this.Label2_Click);
            label20.Click += new System.EventHandler(this.Label2_Click);
            label21.Click += new System.EventHandler(this.Label2_Click);
            label23.Click += new System.EventHandler(this.Label2_Click);
            label24.Click += new System.EventHandler(this.Label2_Click);
        }

        private void Label1_Click(object sender, EventArgs e)
        {
            Label label = (Label)sender;
            label22.Text = (string)label.Tag;
        }

        private void Label2_Click(object sender, EventArgs e)
        {
            Label label = (Label)sender;
            if (label22.Text == "")
            {
                return;
            }
            string[] tagArray = label22.Text.Split(',');
            int x = Convert.ToInt32(tagArray[0]);
            int y = Convert.ToInt32(tagArray[1]);
            array[x, y].Text = label.Text;
        }


        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            label22.Text = "";
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            int n = 5;
            //获得所有需要填充的数据
            Dictionary<string, Label> dict = new Dictionary<string, Label>();
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (array[i, j].Text == "空")
                    {
                        dict.Add(i + "," + j, array[i, j]);
                    }
                }
            }
            int[,] mp = new int[n, n];
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    string str = array[i, j].Text;
                    if (str == "空")
                        mp[i, j] = -1;
                    else if (str == "▲")
                    {
                        mp[i, j] = 1;
                    }
                    else if (str == "■")
                    {
                        mp[i, j] = 2;
                    }
                    else if (str == "✚")
                    {
                        mp[i, j] = 3;
                    }
                    else if (str == "●")
                    {
                        mp[i, j] = 4;
                    }
                    else
                    {
                        mp[i, j] = 5;
                    }
                }
            }
            Calc calc = new Calc(mp, n);
            mp = calc.getResult();
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    int num = mp[i, j];
                    if (num == -1)
                    {
                        array[i, j].Text = "空";
                    }
                    else if (num == 1)
                    {
                        array[i, j].Text = "▲";
                    }
                    else if (num == 2)
                    {
                        array[i, j].Text = "■";
                    }
                    else if (num == 3)
                    {
                        array[i, j].Text = "✚";
                    }
                    else if (num == 4)
                    {
                        array[i, j].Text = "●";
                    }
                    else
                    {
                        array[i, j].Text = "★";
                    }
                    if (dict.Keys.Contains(i + "," + j))
                    {
                        array[i, j].ForeColor = Color.Red;
                    }

                }
            }


        }

        private void Button2_Click(object sender, EventArgs e)
        {
            int n = 5;
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    array[i, j].Text = "空";
                    array[i, j].ForeColor = Color.Black;
                }
            }

        }

        private void Form2_KeyDown(object sender, KeyEventArgs e)
        {
            if (label22.Text.Length == 0)
            {
                return;
            }
            string[] tagArray = label22.Text.Split(',');
            int x = Convert.ToInt32(tagArray[0]);
            int y = Convert.ToInt32(tagArray[1]);
            if (e.KeyCode == Keys.NumPad0 || e.KeyCode == Keys.K || e.KeyCode == Keys.N)
            {
                array[x, y].Text = "空";
            }
            else if (e.KeyCode == Keys.NumPad1 || e.KeyCode == Keys.Y)
            {
                array[x, y].Text = "●";
            }
            else if (e.KeyCode == Keys.NumPad2 || e.KeyCode == Keys.J)
            {
                array[x, y].Text = "✚";
            }
            else if (e.KeyCode == Keys.NumPad3 || e.KeyCode == Keys.S)
            {
                array[x, y].Text = "▲";
            }
            else if (e.KeyCode == Keys.NumPad4 || e.KeyCode == Keys.Z || e.KeyCode == Keys.F)
            {
                array[x, y].Text = "■";
            }
            else if (e.KeyCode == Keys.NumPad5 || e.KeyCode == Keys.W)
            {
                array[x, y].Text = "★";
            }
        }
    }
}
