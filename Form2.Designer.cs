﻿namespace 图形数独
{
    partial class Form2
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form2));
            this.label17 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label21 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(13, 13);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(52, 15);
            this.label17.TabIndex = 16;
            this.label17.Text = "已选择";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(71, 13);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(0, 15);
            this.label22.TabIndex = 21;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(693, 172);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 37);
            this.button1.TabIndex = 22;
            this.button1.Text = "计算";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(693, 282);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 38);
            this.button2.TabIndex = 23;
            this.button2.Text = "清空";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Button2_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.label24);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.label23);
            this.groupBox1.Location = new System.Drawing.Point(32, 476);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(736, 95);
            this.groupBox1.TabIndex = 24;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "填充";
            // 
            // label21
            // 
            this.label21.Font = new System.Drawing.Font("宋体", 20F);
            this.label21.Location = new System.Drawing.Point(424, 21);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(71, 59);
            this.label21.TabIndex = 69;
            this.label21.Text = "●";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.Font = new System.Drawing.Font("宋体", 20F);
            this.label24.Location = new System.Drawing.Point(545, 21);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(71, 59);
            this.label24.TabIndex = 66;
            this.label24.Text = "★";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.Font = new System.Drawing.Font("宋体", 20F);
            this.label19.Location = new System.Drawing.Point(176, 17);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(71, 59);
            this.label19.TabIndex = 65;
            this.label19.Text = "■";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.Font = new System.Drawing.Font("宋体", 20F);
            this.label18.Location = new System.Drawing.Point(24, 17);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(71, 59);
            this.label18.TabIndex = 63;
            this.label18.Text = "▲";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.Font = new System.Drawing.Font("宋体", 20F);
            this.label20.Location = new System.Drawing.Point(317, 17);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(71, 59);
            this.label20.TabIndex = 62;
            this.label20.Text = "✚";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.Font = new System.Drawing.Font("宋体", 20F);
            this.label23.Location = new System.Drawing.Point(645, 21);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(71, 59);
            this.label23.TabIndex = 57;
            this.label23.Text = "空";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("宋体", 20F);
            this.label1.Location = new System.Drawing.Point(56, 51);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 59);
            this.label1.TabIndex = 43;
            this.label1.Text = "✚";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("宋体", 20F);
            this.label2.Location = new System.Drawing.Point(208, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(71, 59);
            this.label2.TabIndex = 44;
            this.label2.Text = "▲";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("宋体", 20F);
            this.label3.Location = new System.Drawing.Point(340, 51);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 59);
            this.label3.TabIndex = 45;
            this.label3.Text = "空";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("宋体", 20F);
            this.label4.Location = new System.Drawing.Point(461, 51);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 59);
            this.label4.TabIndex = 46;
            this.label4.Text = "空";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.Font = new System.Drawing.Font("宋体", 20F);
            this.label25.Location = new System.Drawing.Point(577, 51);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(71, 59);
            this.label25.TabIndex = 47;
            this.label25.Text = "空";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("宋体", 20F);
            this.label5.Location = new System.Drawing.Point(56, 137);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 59);
            this.label5.TabIndex = 48;
            this.label5.Text = "空";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("宋体", 20F);
            this.label6.Location = new System.Drawing.Point(208, 137);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(71, 59);
            this.label6.TabIndex = 49;
            this.label6.Text = "空";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("宋体", 20F);
            this.label8.Location = new System.Drawing.Point(461, 137);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(71, 59);
            this.label8.TabIndex = 50;
            this.label8.Text = "空";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("宋体", 20F);
            this.label10.Location = new System.Drawing.Point(208, 230);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(71, 59);
            this.label10.TabIndex = 51;
            this.label10.Text = "空";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("宋体", 20F);
            this.label12.Location = new System.Drawing.Point(456, 230);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(71, 59);
            this.label12.TabIndex = 52;
            this.label12.Text = "空";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("宋体", 20F);
            this.label14.Location = new System.Drawing.Point(208, 314);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(71, 59);
            this.label14.TabIndex = 53;
            this.label14.Text = "空";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.Font = new System.Drawing.Font("宋体", 20F);
            this.label15.Location = new System.Drawing.Point(340, 314);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(71, 59);
            this.label15.TabIndex = 54;
            this.label15.Text = "空";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label28
            // 
            this.label28.Font = new System.Drawing.Font("宋体", 20F);
            this.label28.Location = new System.Drawing.Point(577, 314);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(71, 59);
            this.label28.TabIndex = 55;
            this.label28.Text = "空";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label29
            // 
            this.label29.Font = new System.Drawing.Font("宋体", 20F);
            this.label29.Location = new System.Drawing.Point(56, 391);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(71, 59);
            this.label29.TabIndex = 56;
            this.label29.Text = "空";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label30
            // 
            this.label30.Font = new System.Drawing.Font("宋体", 20F);
            this.label30.Location = new System.Drawing.Point(208, 391);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(71, 59);
            this.label30.TabIndex = 57;
            this.label30.Text = "空";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label31
            // 
            this.label31.Font = new System.Drawing.Font("宋体", 20F);
            this.label31.Location = new System.Drawing.Point(340, 391);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(71, 59);
            this.label31.TabIndex = 58;
            this.label31.Text = "空";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label32
            // 
            this.label32.Font = new System.Drawing.Font("宋体", 20F);
            this.label32.Location = new System.Drawing.Point(456, 391);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(71, 59);
            this.label32.TabIndex = 59;
            this.label32.Text = "空";
            this.label32.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label33
            // 
            this.label33.Font = new System.Drawing.Font("宋体", 20F);
            this.label33.Location = new System.Drawing.Point(577, 391);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(71, 59);
            this.label33.TabIndex = 60;
            this.label33.Text = "空";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label27
            // 
            this.label27.Font = new System.Drawing.Font("宋体", 20F);
            this.label27.Location = new System.Drawing.Point(577, 230);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(71, 59);
            this.label27.TabIndex = 61;
            this.label27.Text = "✚";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("宋体", 20F);
            this.label11.Location = new System.Drawing.Point(340, 230);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(71, 59);
            this.label11.TabIndex = 62;
            this.label11.Text = "▲";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("宋体", 20F);
            this.label7.Location = new System.Drawing.Point(340, 137);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(71, 59);
            this.label7.TabIndex = 64;
            this.label7.Text = "■";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label26
            // 
            this.label26.Font = new System.Drawing.Font("宋体", 20F);
            this.label26.Location = new System.Drawing.Point(577, 137);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(71, 59);
            this.label26.TabIndex = 65;
            this.label26.Text = "★";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("宋体", 20F);
            this.label9.Location = new System.Drawing.Point(56, 230);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(71, 59);
            this.label9.TabIndex = 66;
            this.label9.Text = "■";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.Font = new System.Drawing.Font("宋体", 20F);
            this.label16.Location = new System.Drawing.Point(456, 314);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(71, 59);
            this.label16.TabIndex = 67;
            this.label16.Text = "■";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("宋体", 20F);
            this.label13.Location = new System.Drawing.Point(56, 314);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(71, 59);
            this.label13.TabIndex = 68;
            this.label13.Text = "●";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Form2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(822, 583);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label27);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label17);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Name = "Form2";
            this.Text = "5*5";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form2_KeyDown);
            this.MouseClick += new System.Windows.Forms.MouseEventHandler(this.Form1_MouseClick);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label21;
    }
}

